import { Component, OnInit } from '@angular/core';

import { Device } from '../points/device';
import { LinkStation } from '../points/link-station';
import { Point } from '../points/point';

@Component({
  selector: 'app-power-grid',
  templateUrl: './power-grid.component.html',
  styleUrls: ['./power-grid.component.scss']
})
export class PowerGridComponent implements OnInit {

  public board: Point[] = [];

  constructor() { }

  public ngOnInit(): void {
    this.initLinkStations();
    this.initDevices();
  }

  public isDevice(point: Point) {
    return point.constructor.name === Device.name;
  }

  public getOptimalLinkStationMessage(point: Point) {
    const stationPower = this.getOptimalLinkStation(point);
    if (stationPower) {
      return `Best link station for point ${point.x}, ${point.y} is ${stationPower.linkStation.x}, ${stationPower.linkStation.y} with power ${stationPower.power.toFixed(2)}`;
    }

    return `No link station within reach for point ${point.x}, ${point.y}`;
  }

  public getOptimalLinkStation(point: Point) {
    const linkStations = this.board.filter((field) => field.constructor.name === LinkStation.name) as LinkStation[];
    return (point as Device).findOptimalLinkStation(linkStations);
  }

  public modifyPoint(point: Point, index: number) {
    if (point) {
      this.board[index] = point;
    } else {
      this.board.splice(index, 1);
    }
  }

  public addPoint(point: Point) {
    this.board.push(point);
  }

  private initLinkStations() {
    this.board.push(new LinkStation(0, 0, 10));
    this.board.push(new LinkStation(20, 20, 5));
    this.board.push(new LinkStation(10, 0, 12));
  }

  private initDevices() {
    this.board.push(new Device(0, 0));
    this.board.push(new Device(100, 100));
    this.board.push(new Device(15, 10));
    this.board.push(new Device(18, 18));
  }
}

