import { Device } from './device';
import { LinkStation } from './link-station';

describe('DeviceClass', () => {

  function getSampleLinkStations() {
    return [
      new LinkStation(0, 0, 10),
      new LinkStation(20, 20, 5),
      new LinkStation(10, 0, 12)
    ];
  }

  it('should properly create', () => {
    for (let index = 0; index < 10; index++) {
      const point = new Device(index, index);
      expect(point).toBeTruthy();
      expect(point.x).toBe(index);
      expect(point.y).toBe(index);
      expect(point.constructor.name).toBe("Device");
    }
  });

  it('should return station on same place as device as optimal', () => {
    const stations = getSampleLinkStations();
    const device = new Device(0, 0);
    const station = device.findOptimalLinkStation(stations);
    expect(station).toBeTruthy();
    expect(station?.linkStation.x).toBe(device.x);
    expect(station?.linkStation.y).toBe(device.y);
  });

  it('should return undefined when no stations are in reach', () => {
    const stations = getSampleLinkStations();
    const device = new Device(100, 100);
    const station = device.findOptimalLinkStation(stations);
    expect(station).toBeUndefined();
  });

  it('should return station with most power', () => {
    const stations = getSampleLinkStations();
    const device = new Device(5, 5);
    const station = device.findOptimalLinkStation(stations);
    expect(station).toBeTruthy();
    expect(station?.linkStation.x).toBe(10);
    expect(station?.linkStation.y).toBe(0);
  });

});
