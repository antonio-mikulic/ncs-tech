import { LinkStation } from './link-station';
import { Point } from './point';

export interface LinkStationPower {
    linkStation: LinkStation;
    power: number;
    point: Point;
  }