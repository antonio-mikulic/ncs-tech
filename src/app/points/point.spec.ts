import { Point } from './point';

describe('PointClass', () => {

  it('should properly create', () => {
    for (let index = 0; index < 10; index++) {
      const point = new Point(index, index);
      expect(point).toBeTruthy();
      expect(point.x).toBe(index);
      expect(point.y).toBe(index);
      expect(point.constructor.name).toBe("Point");
    }
  });

  it('should return distance of 0 on same coordinates', () => {
    const point = new Point(5, 5);
    const distance = point.getDistanceToPoint(point);
    expect(distance).toBe(0);
  });

  it('should properly calculate distance on same axis', () => {
    const point = new Point(5, 5);
    const point2 = new Point(10, 5);
    const distance = point.getDistanceToPoint(point2);
    expect(distance).toBe(5);
  });

  it('should properly calculate distance', () => {
    const point = new Point(4, 4);
    const point2 = new Point(15, 15);
    const distance = point.getDistanceToPoint(point2).toFixed(2);
    expect(distance).toBeCloseTo(15.56);
  });

});
