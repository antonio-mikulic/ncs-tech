import { Point } from './point';

export class LinkStation extends Point {
    public reach: number;
  
    constructor(x: number, y: number, reach: number) {
      super(x, y);
      this.reach = reach;
    }
  
    public getPowerAtPoint(point: Point) {
      const distance = this.getDistanceToPoint(point);
      const isWithinReach = distance < this.reach;
  
      return isWithinReach ? Math.pow((this.reach - distance), 2) : 0;
    }
  }
  
  