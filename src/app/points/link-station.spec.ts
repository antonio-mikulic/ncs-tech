import { LinkStation } from './link-station';
import { Point } from './point';

describe('LinkStationClass', () => {

  it('should properly create', () => {
    for (let index = 0; index < 10; index++) {
      const point = new LinkStation(index, index, index);
      expect(point).toBeTruthy();
      expect(point.x).toBe(index);
      expect(point.y).toBe(index);
      expect(point.reach).toBe(index);
      expect(point.constructor.name).toBe("LinkStation");
    }
  });

  it('power out of reach should be 0', () => {
    const station = new LinkStation(0, 0, 5);
    const point = new Point(10, 10);
    const power = station.getPowerAtPoint(point);
    expect(power).toBe(0);
  });

  it('power at same point should be reach squared', () => {
    const station = new LinkStation(5, 5, 5);
    const point = new Point(5, 5);
    const power = station.getPowerAtPoint(point);
    expect(power).toBe(25);
  });

  it('should properly calculate power', () => {
    const station = new LinkStation(0, 0, 10);
    const point = new Point(5, 5);
    const power = station.getPowerAtPoint(point).toFixed(2);
    expect(power).toBeCloseTo(8.58);
  });

});
