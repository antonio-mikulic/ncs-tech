import { LinkStation } from './link-station';
import { LinkStationPower } from './link-station-power';
import { Point } from './point';

export class Device extends Point {
  constructor(x: number, y: number) {
    super(x, y);
  }

  public findOptimalLinkStation(stations: LinkStation[]): LinkStationPower | undefined {
    const calculatedPowers: LinkStationPower[] = stations.map((station) => {
      return {
        linkStation: station,
        point: this,
        power: station.getPowerAtPoint(this)
      };
    });

    const optimal = calculatedPowers.reduce((max, game) => max.power > game.power ? max : game);

    if (optimal.power > 0) {
      return optimal;
    }

    return undefined;
  }
}

