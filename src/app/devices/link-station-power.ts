export interface LinkStationPower {
    linkStation: LinkStation;
    power: number;
    point: Point;
  }