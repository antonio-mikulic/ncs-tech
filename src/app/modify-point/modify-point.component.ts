import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Device } from '../points/device';
import { LinkStation } from '../points/link-station';
import { Point } from '../points/point';

@Component({
  selector: 'app-modify-point',
  templateUrl: './modify-point.component.html',
  styleUrls: ['./modify-point.component.scss']
})
export class ModifyPointComponent {

  private _point?: Point;

  @Input() public set point(point: Point | undefined) {
    this.setPoint(point);
  }

  public get point() {
    return this._point;
  }

  @Output() public onPointModified = new EventEmitter<Point>();

  public pointTypes: string[] = [Point.name, Device.name, LinkStation.name];

  public pointForm: FormGroup;
  public yControl: FormControl;
  public xControl: FormControl;
  public typeControl: FormControl;
  public reachControl: FormControl;

  constructor() {
    this.yControl = new FormControl([Validators.required]);
    this.xControl = new FormControl([Validators.required]);
    this.typeControl = new FormControl([Validators.required]);
    this.reachControl = new FormControl();

    this.pointForm = new FormGroup({
      x: this.yControl,
      y: this.xControl,
      type: this.typeControl,
      reach: this.reachControl
    })
  }

  public get isValid() {
    return this.pointForm.valid && (!this.hasReach || this.reachControl.value > 0);
  }

  public get hasReach() {
    return this.typeControl.valid && this.typeControl.value === LinkStation.name;
  }

  public getPointTypeButtonColor(type: string) {
    return type === this.typeControl.value ? "accent" : "";
  }

  public updatePointType(type: string) {
    this.typeControl.setValue(type);
  }

  private setPoint(point?: Point) {
    if (!point) { return; }

    if (point.constructor.name === LinkStation.name) {
      this.reachControl.setValue((point as LinkStation).reach);
    }

    this.xControl.setValue(point.x);
    this.yControl.setValue(point.y);
    this.typeControl.setValue(point.constructor.name);
    this._point = point;
  }

  public updatePoint() {
    let point: Point | undefined;

    if (!this.pointForm.valid) {
      return;
    }

    switch (this.typeControl.value) {
      case Point.name:
        point = new Point(this.xControl.value, this.yControl.value);
        break;
      case Device.name:
        point = new Device(this.xControl.value, this.yControl.value);
        break;
      case LinkStation.name:
        point = new LinkStation(this.xControl.value, this.yControl.value, this.reachControl.value);
        break;
      default:
        point = undefined;
    }

    if (point) {
      this.onPointModified.emit(point);
    }

  }

  public deletePoint() {
    this.onPointModified.emit(undefined);
  }

}
