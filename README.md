## Local setup guide
- Install [Node.js](https://nodejs.org/en/). Verison 14.17.1 LTS reccomended
- Install [Angular CLI](https://angular.io/cli)
- Open terminal in project root folder
- Run "npm install"
- Wait for install to finish
- Run "npm run start"
- Open [browser](http://localhost:4200/)

## Run tests
- Tests can be run after local setup is done
- Run with "ng test"

## Guide
- Upon opening a list of points is shown
- First row is used for adding new points
- Point can be of type Point, Device and Link Station
- Points can be updated and removed at any time
- Link stations have a range and all devices in that range can be powered

## Possible improvements
- More unit tests
- Go over function names and ensure they are easy to read
- Improve UI
  - Update/Delete buttons shouldn't be this close
  - Use better colors, dark mode
- Extract math to a math helper class